# RPN-Brute-Force-Parser

RPN-Brute-Force-Parser is a simple parser for propositional logic with reverse
Polish notation built with JavaScript and React. It parses the following
grammar:

```ebnf
<formula>       ::= <proposition> | <negation> | <conjunction> | <disjunction> | <conditional> | <biconditional>
<negation>      ::= <formula> "N"
<conjunction>   ::= <formula> <formula> "K"
<disjunction>   ::= <formula> <formula> "A"
<conditional>   ::= <formula> <formula> "C"
<biconditional> ::= <formula> <formula> "E"
<proposition>   ::= [a-z]
```

You can chek it out in action here:

https://writeonlycode.gitlab.io/rpn-brute-force-parser/

![Screenshot](public/image.png)

## How it Works Behind the Scenes 

TODO

## Installation and Deployment

This project was bootstrapped with [Create React
App](https://github.com/facebook/create-react-app). In the project directory,
you can run:

### `npm start`

Runs the app in the development mode. Open
[http://localhost:3000](http://localhost:3000) to view it in your browser. The
page will reload when you make changes. You may also see any lint errors in the
console.

### `npm run build`

Builds the app for production to the `build` folder. It correctly bundles React
in production mode and optimizes the build for the best performance. The build
is minified and the filenames include the hashes. Your app is ready to be
deployed! See the section about
[deployment](https://facebook.github.io/create-react-app/docs/deployment) for
more information.
