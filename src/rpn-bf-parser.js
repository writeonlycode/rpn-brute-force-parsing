export function parser(input) {
  const inputTokenized = lexer(input);
  // It has to generate all the sentences until it finds either: the string we
  // are trying to parse, or a string longer than the string we are trying to
  // parse.
  return;
}

function lexer(input) {
  const inputTokenized = [];

  if (typeof input !== "string")
    return;

  for (let c of input) {
    if (PropositonSymbol.isMatch(c)) {
      inputTokenized.push(new PropositonSymbol(c));
    } else if (NegationSymbol.isMatch(c)) {
      inputTokenized.push(new NegationSymbol(c));
    } else if (ConjunctionSymbol.isMatch(c)) {
      inputTokenized.push(new ConjunctionSymbol(c));
    } else if (DisjunctionSymbol.isMatch(c)) {
      inputTokenized.push(new DisjunctionSymbol(c));
    } else if (ConditionalSymbol.isMatch(c)) {
      inputTokenized.push(new ConditionalSymbol(c));
    } else if (BiconditionalSymbol.isMatch(c)) {
      inputTokenized.push(new BiconditionalSymbol(c));
    } else if (c === " ") {
      // skip if it's a space
    } else {
      throw new SyntaxError(`The symbol "${c}" is not part of the lexicon.`);
    }
  }

  return inputTokenized;
}

class LanguageSymbol {
  constructor(symbol) {
    this.symbol = symbol;
  }

  static isMatch(input) {
    if ( this.symbols.includes(input) ) {
      return true;
    } else {
      return false;
    }
  }
}

class ConnectiveSymbol extends LanguageSymbol {
  constructor(symbol) {
    super(symbol);
    this.children = [];
  }
}

class PropositonSymbol extends LanguageSymbol { static symbols = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]; }

class UnaryConnectiveSymbol extends ConnectiveSymbol { }
class NegationSymbol extends UnaryConnectiveSymbol { static symbols = ["N"] }

class BinaryConnectiveSymbol extends ConnectiveSymbol { }
class ConjunctionSymbol extends BinaryConnectiveSymbol { static symbols = ["K"] }
class DisjunctionSymbol extends BinaryConnectiveSymbol { static symbols = ["A"] }
class ConditionalSymbol extends BinaryConnectiveSymbol { static symbols = ["C"] }
class BiconditionalSymbol extends BinaryConnectiveSymbol { static symbols = ["E"] }

class SyntaxError extends Error {
  constructor(message) {
    super(message);
    this.name = 'Syntax Error';
  }
}
